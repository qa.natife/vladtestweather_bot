package org.example;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

public class Main {
    public static void main(String[] args) throws TelegramApiException {
        // Створюємо об'єкт TelegramBotsApi
        //TelegramBotsApi botsApi = new TelegramBotsApi();
        TelegramBotsApi botsApi = null;
        try {
            botsApi = new TelegramBotsApi(DefaultBotSession.class);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
        try {
            // Реєструємо вашого бота
            botsApi.registerBot(new MyTelegramBot());
            System.out.println("Бот запущено!");
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}

