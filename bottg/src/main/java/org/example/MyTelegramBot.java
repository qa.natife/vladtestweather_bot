package org.example;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class MyTelegramBot extends TelegramLongPollingBot {

    @Override
    public String getBotUsername() {
        // Повертаємо ім'я вашого бота
        return "vladtestweather_bot";
    }

    @Override
    public String getBotToken() {
        // Повертаємо токен вашого бота
        return "6953627057:AAFKZHJkkq4wi050vSCCsVfk9zbcEdA_oOI";
    }

    @Override
    public void onUpdateReceived(Update update) {
        // Обробляємо отримані повідомлення тут
    }

    public void sendMessage(String chatId, String text) {
        // Створюємо об'єкт SendMessage з необхідною інформацією
        SendMessage message = new SendMessage();
        message.setChatId(chatId);
        message.setText(text);

        // Відправляємо повідомлення за допомогою методу execute
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
